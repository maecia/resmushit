<?php

/**
 * @file
 * Resmushit.api.php.
 *
 * Hooks provided by the Resmush.it module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter an image being optimized by resmush.it.
 *
 * @param object $image
 *   An image object.
 * @param array $context
 *   Additional parameters.
 *
 * @see image_load()
 * @see image_imagemagick_load()
 */
function hook_resmushit_optimize_alter(stdClass $image, $context) {
  // Set the optimized flag ON so we do not process the image twice.
  $image->optimized = TRUE;

  // Do your custom alterations
  // ...
}
