<?php

/**
 * @file
 * Resmushit.module.
 *
 * The Drupal Resmush.it Module provides a simple intergration between the
 * image processing service http://resmush.it and Drupal.
 */

define("RESMUSHIT_ENDPOINT", 'http://api.resmush.it/');

/**
 * Implements hook_image_toolkits().
 */
function resmushit_image_toolkits() {
  return array(
    'resmushit' => array(
      'title' => t('Resmush.it image compression service'),
      'available' => TRUE,
    ),
  );
}

/**
 * Implements image_TOOLKIT_settings().
 *
 * Settings form for Resmush.it toolkit.
 */
function image_resmushit_settings() {

  $form['resmushit_service'] = array(
    '#type' => 'fieldset',
    '#title' => t('Resmush.it'),
  );

  // Get available toolkits.
  $toolkits = image_get_available_toolkits();

  // Remove resmushit toolkit.
  unset($toolkits['resmushit']);

  $form['resmushit_service']['resmushit_base_toolkit'] = array(
    '#type' => 'select',
    '#title' => t('Base toolkit to use'),
    '#required' => TRUE,
    '#options' => $toolkits,
    '#description' => t('Resmushit will optimize images processed by the selected toolkit.'),
    '#default_value' => variable_get('resmushit_base_toolkit', 'gd'),
  );

  $form['resmushit_service']['resmushit_service_quality'] = array(
    '#type' => 'textfield',
    '#title' => t('JPEG Quality'),
    '#description' => t('The desired quality of the generated JPEG files. Range 0 - 100%'),
    '#default_value' => variable_get('resmushit_service_quality', 80),
    '#maxlength' => 3,
    '#size' => 5,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  return $form;
}

/**
 * Scales an image to the specified size.
 *
 * @param object $image
 *   An image object. The $image->resource, $image->info['width'], and
 *   $image->info['height'] values will be modified by this call.
 * @param int $width
 *   The new width of the resized image, in pixels.
 * @param int $height
 *   The new height of the resized image, in pixels.
 *
 * @return bool
 *   TRUE or FALSE, based on success.
 *
 * @see image_resize()
 */
function image_resmushit_resize(stdClass $image, $width, $height) {
  return _image_resmushit_invoke('resize', $image, array($width, $height));
}

/**
 * Rotates an image the given number of degrees.
 *
 * @param object $image
 *   An image object. The $image->resource, $image->info['width'], and
 *   $image->info['height'] values will be modified by this call.
 * @param int $degrees
 *   The number of (clockwise) degrees to rotate the image.
 * @param string $background
 *   An hexadecimal integer specifying the background color to use for the
 *   uncovered area of the image after the rotation. E.g. 0x000000 for black,
 *   0xff00ff for magenta, and 0xffffff for white. For images that support
 *   transparency, this will default to transparent. Otherwise it will
 *   be white.
 *
 * @return bool
 *   TRUE or FALSE, based on success.
 *
 * @see image_rotate()
 */
function image_resmushit_rotate(stdClass $image, $degrees, $background = NULL) {
  return _image_resmushit_invoke('rotate', $image, array($degrees, $background));
}

/**
 * Converts an image into grayscale.
 *
 * @param object $image
 *   An image object. The $image->resource value will be modified by this call.
 *
 * @return bool
 *   TRUE or FALSE, based on success.
 *
 * @see image_desaturate()
 */
function image_resmushit_desaturate(stdClass $image) {
  return _image_resmushit_invoke('desaturate', $image);
}

/**
 * Crops an image to the given coordinates.
 *
 * @param object $image
 *   An image object. The $image->resource, $image->info['width'], and
 *   $image->info['height'] values will be modified by this call.
 * @param int $x
 *   The starting x offset at which to start the crop, in pixels.
 * @param int $y
 *   The starting y offset at which to start the crop, in pixels.
 * @param int $width
 *   The width of the cropped area, in pixels.
 * @param int $height
 *   The height of the cropped area, in pixels.
 *
 * @return bool
 *   TRUE or FALSE, based on success.
 *
 * @see image_crop()
 */
function image_resmushit_crop(stdClass $image, $x, $y, $width, $height) {
  return _image_resmushit_invoke('crop', $image, array($x, $y, $width, $height));
}

/**
 * Get details about an image.
 *
 * @param object $image
 *   An image object.
 *
 * @return bool
 *   FALSE, if the file could not be found or is not an image. Otherwise, a
 *   keyed array containing information about the image:
 *   - width: Width in pixels.
 *   - height: Height in pixels.
 *   - extension: Commonly used file extension for the image.
 *   - mime_type: MIME type ('image/jpeg', 'image/gif', 'image/png').
 *
 * @see image_get_info()
 */
function image_resmushit_get_info(stdClass $image) {
  return _image_resmushit_invoke('get_info', $image);
}

/**
 * Creates an image resource from a file.
 *
 * @param object $image
 *   An image object. The $image->resource value will populated by this call.
 *
 * @return bool
 *   TRUE or FALSE, based on success.
 *
 * @see image_load()
 */
function image_resmushit_load(stdClass $image) {
  return _image_resmushit_invoke('load', $image);
}

/**
 * Implements image_TOOLKIT_save().
 *
 * @param object $image
 *   An image object.
 * @param string $dst
 *   The destination uri for this file.
 *
 * @return bool
 *   TRUE or FALSE, based on success.
 */
function image_resmushit_save(stdClass $image, $dst) {
  if (_image_resmushit_invoke('save', $image, array($dst))) {
    // Set a flag to check weither the image was processed elsewhere.
    $image->optimized = FALSE;

    // Call registered hooks.
    $context = array(
      'destination' => &$dst,
    );

    drupal_alter('resmushit_optimize', $image, $context);
    return _resmushit_optimize($image, $dst);
  }
}

/**
 * Helper. Based on image_toolkit_invoke().
 *
 * @param string $method
 *   The operation to apply on the image.
 * @param object $image
 *   An image object.
 * @param array $params
 *   Additional params passed to the operation.
 */
function _image_resmushit_invoke($method, $image, array $params = array()) {
  $function = 'image_' . variable_get('resmushit_base_toolkit', 'gd') . '_' . $method;
  if (function_exists($function)) {
    array_unshift($params, $image);
    return call_user_func_array($function, $params);
  }
  return FALSE;
}

/**
 * Optimizes image with external commands.
 *
 * @param object $image
 *   An image object.
 * @param string $dst
 *   Destination URI for this image.
 */
function _resmushit_optimize($image, $dst) {
  if ($image->optimized) {
    return TRUE;
  }

  global $base_url;
  $version = system_get_info('module', 'resmushit')['version'];

  if (!function_exists('json_decode')) {
    drupal_set_message(t('Required function, json_decode(), is not available.'), 'error');
    return FALSE;
  }

  $dst = drupal_realpath($dst);

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, RESMUSHIT_ENDPOINT);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
  curl_setopt($ch, CURLOPT_POST, TRUE);
  curl_setopt($ch, CURLOPT_USERAGENT, "Drupal " . VERSION . " /Resmush.it $version - $base_url");

  if (!class_exists('CURLFile')) {
    $arg = array('files' => '@' . $dst);
  }
  else {
    $cfile = new CURLFile($dst);
    $arg = array(
      'files' => $cfile,
    );
  }

  // Add the quality parameter as a POST parameter.
  $arg['qlty'] = variable_get('resmushit_service_quality', 80);

  curl_setopt($ch, CURLOPT_POSTFIELDS, $arg);

  $data = curl_exec($ch);
  curl_close($ch);

  $json = json_decode($data);

  // ReSmushIt returns an error if it cannot optimize the image. Otherwise, it
  // returns an object, with 'dest' (temporary file) and 'percent' (savings)
  // among other properties.
  if (!isset($json->error) && isset($json->dest)) {
    $result = drupal_http_request($json->dest);
    if (!isset($result->error)) {
      file_unmanaged_save_data($result->data, $dst, FILE_EXISTS_REPLACE);
      return TRUE;
    }
  }
  else {
    watchdog('Resmush.it', 'Error when optimizing image : <pre>@image</pre> with infos : <pre>@json</pre>', array(
      '@image' => print_r($image, TRUE),
      '@json' => print_r($json, TRUE),
    ), WATCHDOG_ALERT, 'link');
  }

  // If optimize service fails, there is no problem. Original image is saved.
  return TRUE;
}
